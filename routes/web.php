<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});
Route::get('/products',function(){
    return view('frontend.product');
});
Route::get('/cart',function(){
    return view('frontend.cart');
});
Route::get('/checkout',function(){
    return view('frontend.checkout');
});

//backend
Route::group(['prefix' => "/admin"], function () {
    Route::get('/',function(){
        return view('backend.dashboard');
    });
    Route::get('/categories',function(){
        return view('backend.category');
    });
    Route::get('/products',function(){
        return view('backend.listproduct');
    });
    Route::get('/order',function(){
        return view('backend.order');
    });
    Route::get('/listuser',function(){
        return view('backend.listuser');
    });
});

